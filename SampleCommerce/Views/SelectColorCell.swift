//
//  SelectColorCell.swift
//  SampleCommerce
//
//  Created by Macintosh on 11/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class SelectColorCell: UITableViewCell {

    @IBOutlet weak var btnHitam: UIButton!
    
    @IBOutlet weak var btnBiru: UIButton!
    
    @IBOutlet weak var btnCoklat: UIButton!
    
    @IBOutlet weak var btnMerah: UIButton!
    
    @IBOutlet weak var btnKategori: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnKategori.layer.cornerRadius = 8
        btnKategori.setTitleColor(.white, for: .normal)
        btnKategori.layer.backgroundColor = UIColor.init(hexString: "58b2e1").cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(variant: String, kategori: String) {
        btnKategori.setTitle(kategori, for: .normal)
        switch variant {
        case "Coklat":
            btnCoklat.selectButton(title: "COKLAT")
            btnBiru.unSelectButton(title: "BIRU")
            btnHitam.unSelectButton(title: "HITAM")
            btnMerah.unSelectButton(title: "MERAH")
        case "Biru":
            btnCoklat.unSelectButton(title: "COKLAT")
            btnBiru.selectButton(title: "BIRU")
            btnHitam.unSelectButton(title: "HITAM")
            btnMerah.unSelectButton(title: "MERAH")
        case "Hitam":
            btnCoklat.unSelectButton(title: "COKLAT")
            btnBiru.unSelectButton(title: "BIRU")
            btnHitam.selectButton(title: "HITAM")
            btnMerah.unSelectButton(title: "MERAH")
        default:
            btnCoklat.unSelectButton(title: "COKLAT")
            btnBiru.unSelectButton(title: "BIRU")
            btnHitam.unSelectButton(title: "HITAM")
            btnMerah.selectButton(title: "MERAH")
        }
    }
    
}
