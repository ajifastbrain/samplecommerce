//
//  EmptyCell.swift
//  SampleCommerce
//
//  Created by Macintosh on 11/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class EmptyCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
