//
//  DetailImageViewCell.swift
//  SampleCommerce
//
//  Created by Macintosh on 10/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class DetailImageViewCell: UITableViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelDiscount: UILabel!
    
    @IBOutlet weak var labelPrice: UILabel!
    
    private var images = String()
    
    private var name = String()
    
    private var discount = Int()
    
    private var price = Int()
    
    private var afterDiscount = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(images: String, name: String, discount: Int, price: Int, afterDiscount: Int) {
        self.images = images
        self.name = name
        self.discount = discount
        self.price = price
        self.afterDiscount = afterDiscount
        
        self.mainImage.image = UIImage(named: self.images)
        self.labelName.text = self.name
        
        if discount == 0 {
            labelDiscount.isHidden = true
            
            let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "fc1522")]
            let attributedString2 = NSMutableAttributedString(string: "Rp. " + price.convertToCurrency(), attributes:attrs2 as [NSAttributedString.Key : Any])
            
            let attrs3 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "282828")]
            let attributedString3 = NSMutableAttributedString(string: "/pcs", attributes:attrs3 as [NSAttributedString.Key : Any])
            attributedString2.append(attributedString3)
            self.labelPrice.attributedText = attributedString2

        } else {
            labelDiscount.text = String(discount) + "% OFF"
            labelDiscount.isHidden = false
        
            let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "868686"), .strikethroughStyle: NSUnderlineStyle.single.rawValue] as [NSAttributedString.Key : Any]
            let attributedString1 = NSMutableAttributedString(string: "Rp. " + price.convertToCurrency() + " ", attributes:attrs1 as [NSAttributedString.Key : Any])
            
            let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22), NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "fc1522")]
            let attributedString2 = NSMutableAttributedString(string: "Rp. " + afterDiscount.convertToCurrency(), attributes:attrs2 as [NSAttributedString.Key : Any])
            
            let attrs3 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "282828")]
            let attributedString3 = NSMutableAttributedString(string: "/pcs", attributes:attrs3 as [NSAttributedString.Key : Any])
            attributedString1.append(attributedString2)
            attributedString1.append(attributedString3)
            self.labelPrice.attributedText = attributedString1

        }
        
      
        
    }
    
}
