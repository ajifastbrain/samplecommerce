//
//  ItemsCell.swift
//  SampleCommerce
//
//  Created by Macintosh on 09/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ItemsCell: UICollectionViewCell {
    
    @IBOutlet weak var viewFrame: UIView!
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var labelDiscount: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelName.lineBreakMode = .byWordWrapping
        labelName.numberOfLines = 0
        
        viewFrame.layer.cornerRadius = 10
    }
    
    func configureView(imageName: String, discount: Int, nameItem: String, price: Int) {
        mainImage.image = UIImage(named: imageName)
        if discount == 0 {
            labelDiscount.isHidden = true
        } else {
            labelDiscount.text = String(discount) + "% OFF"
            labelDiscount.isHidden = false
        }
        labelName.text = nameItem
        labelPrice.text = "Rp." + price.convertToCurrency()
    }

}
