//
//  ListBasketCell.swift
//  SampleCommerce
//
//  Created by Macintosh on 17/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ListBasketCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelPrice: UILabel!
    
    @IBOutlet weak var labelDiskon: UILabel!
    
    @IBOutlet weak var labelQty: UILabel!
    
    @IBOutlet weak var labelTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(imgProd: String, name: String, price: Int, diskon: Int, qty: Int) {
        self.mainImage.image = UIImage(named: imgProd)
        self.labelName.text = name
        self.labelPrice.text = "Price : Rp. " + price.convertToCurrency()
        self.labelDiskon.text = "Discount : " + String(diskon) + " %"
        self.labelQty.text = "Qty : " + String(qty) + " pcs"
        self.labelTotal.text = "Rp. " + (qty * price).convertToCurrency()
    }
    
}
