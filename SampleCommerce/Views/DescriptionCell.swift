//
//  DescriptionCell.swift
//  SampleCommerce
//
//  Created by Macintosh on 11/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {
    
    @IBOutlet weak var labelDeskripsi: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelDeskripsi.lineBreakMode = .byWordWrapping
        labelDeskripsi.numberOfLines = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(desc: String) {
        labelDeskripsi.text = desc
    }
    
}
