//
//  AppDelegate.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Util.copyFile(fileName: "ItemsDB.db")
        IQKeyboardManager.shared.enable = true
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let rootVC = HomeViewController()
        
        let navController = BaseNavigationController(rootViewController: rootVC)
        window?.rootViewController = navController
        return true
    }
}

