//
//  StringExtension.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

extension String {
    static func className(_ tClass: AnyClass!) -> String {
        return NSStringFromClass(tClass).components(separatedBy: ".").last!
    }
    
}
