//
//  UITextfieldExtension.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }

    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func roundTextField() {
        layer.cornerRadius = 10
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
        setLeftPaddingPoints(15)
        setRightPaddingPoints(15)
    }
    
    func set(bgColor: UIColor, placeholderTxt: String, placeholderColor: UIColor, txtColor: UIColor) {
        backgroundColor = bgColor
        attributedPlaceholder = NSAttributedString(string: placeholderTxt, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        textColor = txtColor
        let newText = text
        text = newText
    }

}
