//
//  UITableViewCellExtension.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
extension UITableViewCell {
    class func getIdentifier() -> String {
        return String.className(self)
    }
}
