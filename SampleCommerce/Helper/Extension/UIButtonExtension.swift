//
//  UIButtonExtension.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

extension UIButton {
    
    func mainButton() {
        layer.shadowColor = UIColor(hexString: "#000000").cgColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 6)
        
        layer.masksToBounds = false
        layer.cornerRadius = 25
    }
    
    func selectButton(title: String) {
        layer.borderWidth = 1
        layer.borderColor = UIColor.init(hexString: "f3874d").cgColor
        layer.cornerRadius = 8
        setTitleColor(UIColor.init(hexString: "f3874d"), for: .normal)
        setTitle(title, for: .normal)
    }
    
    func unSelectButton(title: String) {
        layer.borderWidth = 1
        layer.borderColor = UIColor.init(hexString: "e7e7e7").cgColor
        layer.cornerRadius = 8
        setTitleColor(UIColor.init(hexString: "000000"), for: .normal)
        setTitle(title, for: .normal)
    }

 }
