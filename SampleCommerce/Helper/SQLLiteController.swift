//
//  SQLLiteController.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

let sharedInstance = SQLLiteController()

class SQLLiteController: NSObject {
    var database: FMDatabase? = nil
    
    class func getInstance() -> SQLLiteController
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath(fileName: "ItemsDB.db"))
        }
        return sharedInstance
    }
    
    func getAllItems() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM products order by id desc", withArgumentsIn: nil)
        let marrDescInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let modelItems : SchemeProducts = SchemeProducts()
                modelItems.id = resultSet.string(forColumn: "id")
                modelItems.name = resultSet.string(forColumn: "name")
                modelItems.category = resultSet.string(forColumn: "category")
                modelItems.variant = resultSet.string(forColumn: "variant")
                modelItems.price = Int(resultSet.int(forColumn: "price"))
                modelItems.images = resultSet.string(forColumn: "images")
                modelItems.discount = Int(resultSet.int(forColumn: "discount"))
                modelItems.pricediscount = Int(resultSet.int(forColumn: "pricediscount"))
                modelItems.descripstions = resultSet.string(forColumn: "descripstions")
                marrDescInfo.add(modelItems)
            }
        }
        sharedInstance.database!.close()
        return marrDescInfo
    }
    
    func getCounItems() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select count(id) as jml from transactions", withArgumentsIn: nil)
        let marrDescInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let modelItems : SchemeTotalItems = SchemeTotalItems()
                modelItems.totItems = Int(resultSet.int(forColumn: "jml"))
                marrDescInfo.add(modelItems)
            }
        }
        sharedInstance.database!.close()
        return marrDescInfo
    }

    func getAllItemByID(modelInItem: SchemeProducts) -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM products where id = ?", withArgumentsIn: [modelInItem.id])
        let marrDescInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let modelItems : SchemeProducts = SchemeProducts()
                modelItems.id = resultSet.string(forColumn: "id")
                modelItems.name = resultSet.string(forColumn: "name")
                modelItems.category = resultSet.string(forColumn: "category")
                modelItems.variant = resultSet.string(forColumn: "variant")
                modelItems.price = Int(resultSet.int(forColumn: "price"))
                modelItems.images = resultSet.string(forColumn: "images")
                modelItems.discount = Int(resultSet.int(forColumn: "discount"))
                modelItems.pricediscount = Int(resultSet.int(forColumn: "pricediscount"))
                modelItems.descripstions = resultSet.string(forColumn: "descripstions")
                marrDescInfo.add(modelItems)
            }
        }
        sharedInstance.database!.close()
        return marrDescInfo
    }

    func getAllBasket(modelInItem: SchemeBasket) -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select a.images, a.name, a.price, a.discount, b.qty from products a left join transactions b on a.id = b.id " +
            "where  b.dateTrans = ?", withArgumentsIn: [modelInItem.dateTrans])
        let marrDescInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let modelItems : SchemeBasket = SchemeBasket()
                modelItems.images = resultSet.string(forColumn: "images")
                modelItems.name = resultSet.string(forColumn: "name")
                modelItems.price = Int(resultSet.int(forColumn: "price"))
                modelItems.discount = Int(resultSet.int(forColumn: "discount"))
                modelItems.qty = Int(resultSet.int(forColumn: "qty"))
                marrDescInfo.add(modelItems)
            }
        }
        sharedInstance.database!.close()
        return marrDescInfo
    }


    func addItems(modelInProduct: SchemeProducts) -> Bool {
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO products (id, name, category, variant, price, images, discount, pricediscount, descripstions) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", withArgumentsIn: [modelInProduct.id, modelInProduct.name, modelInProduct.category, modelInProduct.variant, modelInProduct.price, modelInProduct.images, modelInProduct.discount, modelInProduct.pricediscount, modelInProduct.descripstions])
        sharedInstance.database!.close()
        return isInserted
    }
    
    func addTransaction(modelTrans: SchemeTransactions) -> Bool {
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO transactions (id, qty, dateTrans) VALUES (?, ?, ?)", withArgumentsIn: [modelTrans.id, modelTrans.qty, modelTrans.dateTrans])
        sharedInstance.database!.close()
        return isInserted
    }

    func deleteItems() -> Bool {
        sharedInstance.database!.open()
        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM transactions", withArgumentsIn: nil)
        sharedInstance.database!.close()
        return isDeleted
    }

}
