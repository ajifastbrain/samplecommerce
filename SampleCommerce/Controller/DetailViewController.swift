//
//  DetailViewController.swift
//  SampleCommerce
//
//  Created by Macintosh on 10/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var viewBuy: UIView!
    
    @IBOutlet weak var viewBasket: UIView!
    
    @IBOutlet weak var viewBuying: UIView!
    
    private var id = String()
    
    var arrayProducts = NSMutableArray()
    
    private var arrayCountItems = NSMutableArray()
    
    private var name = String()
    private var category = String()
    private var variant = String()
    private var price = Int()
    private var images = String()
    private var discount = Int()
    private var pricediscount = Int()
    private var descripstions = String()
    
    @IBOutlet weak var mainTable: UITableView!
    
    @IBAction func insertBasketTapped(_ sender: UIButton) {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YYYY"
        
        let mData = SchemeTransactions()
        mData.id = self.id
        mData.qty = 1
        mData.dateTrans = dateFormatter.string(from: date)
        
        let addNewStaff = SQLLiteController.getInstance().addTransaction(modelTrans: mData)
        if addNewStaff {
            self.navigationController?.popViewController(animated: true)
        } else {
            print("New Trans Order Gagal Simpan")
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "58b2e1")
         navigationController?.navigationBar.shadowImage = UIImage()
         navigationController?.navigationBar.layoutIfNeeded()
         self.navigationItem.setHidesBackButton(true, animated: true)
        
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Detail Product"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        self.navigationItem.titleView = label

         
         self.arrayCountItems = NSMutableArray()
         self.arrayCountItems = SQLLiteController.getInstance().getCounItems()
         let allData:SchemeTotalItems = arrayCountItems.object(at: 0) as! SchemeTotalItems
        
             
         let numBadge = String(allData.totItems)
           
         if numBadge == "0" {
             let chartButton = SSBadgeButton()
             chartButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
             chartButton.setImage(UIImage(named: "whiteChart")?.withRenderingMode(.alwaysTemplate), for: .normal)
             chartButton.tintColor = .white
             chartButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
             
             let bellButton = SSBadgeButton()
             bellButton.frame = CGRect(x: 30, y: 0, width: 32, height: 32)
             bellButton.setImage(UIImage(named: "bellWhite")?.withRenderingMode(.alwaysTemplate), for: .normal)
             bellButton.tintColor = .white
             bellButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
                 
             self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: chartButton), UIBarButtonItem(customView: bellButton)]
         } else {
             let chartButton = SSBadgeButton()
                 chartButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                 chartButton.setImage(UIImage(named: "whiteChart")?.withRenderingMode(.alwaysTemplate), for: .normal)
                 chartButton.tintColor = .white
                 chartButton.badge = numBadge
                 chartButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
                
                 let bellButton = SSBadgeButton()
                 bellButton.frame = CGRect(x: 30, y: 0, width: 32, height: 32)
                 bellButton.setImage(UIImage(named: "bellWhite")?.withRenderingMode(.alwaysTemplate), for: .normal)
                 bellButton.tintColor = .white
                 bellButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
                    
                 self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: chartButton), UIBarButtonItem(customView: bellButton)]

            }
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viewSearch.frame
        rectShape.position = self.viewSearch.center
        rectShape.path = UIBezierPath(roundedRect: self.viewSearch.bounds, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 35, height: 35)).cgPath

        self.viewSearch.layer.backgroundColor = UIColor.init(hexString: "73bee3").cgColor
        self.viewSearch.layer.mask = rectShape
        
        let rectShapeB = CAShapeLayer()
        rectShapeB.bounds = self.viewBuy.frame
        rectShapeB.position = self.viewBuy.center
        rectShapeB.path = UIBezierPath(roundedRect: self.viewBuy.bounds, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 35, height: 35)).cgPath

        self.viewBuy.layer.backgroundColor = UIColor.init(hexString: "73bee3").cgColor
        self.viewBuy.layer.mask = rectShapeB
        
        let rectShapeBs = CAShapeLayer()
        rectShapeBs.bounds = self.viewBasket.frame
        rectShapeBs.position = self.viewBasket.center
        rectShapeBs.path = UIBezierPath(roundedRect: self.viewBasket.bounds, byRoundingCorners: [.topLeft], cornerRadii: CGSize(width: 35, height: 35)).cgPath
        self.viewBasket.layer.mask = rectShapeBs

        let rectShapeBy = CAShapeLayer()
        rectShapeBy.bounds = self.viewBuying.frame
        rectShapeBy.position = self.viewBuying.center
        rectShapeBy.path = UIBezierPath(roundedRect: self.viewBuying.bounds, byRoundingCorners: [.topRight], cornerRadii: CGSize(width: 35, height: 35)).cgPath
        self.viewBuying.layer.mask = rectShapeBy

        
        
        mainTable.register(UINib(nibName: "DetailImageViewCell", bundle: nil), forCellReuseIdentifier: "DetailImageViewCell")
        mainTable.register(UINib(nibName: "SelectColorCell", bundle: nil), forCellReuseIdentifier: "SelectColorCell")
        mainTable.register(UINib(nibName: "DescriptionCell", bundle: nil), forCellReuseIdentifier: "DescriptionCell")
        mainTable.register(UINib(nibName: "EmptyCell", bundle: nil), forCellReuseIdentifier: "EmptyCell")
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .singleLine
        mainTable.reloadData()

    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "DetailImageViewCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! DetailImageViewCell
                cell.selectionStyle = .none
                cell.configureView(images: self.images, name: self.name, discount: self.discount, price: self.price, afterDiscount: self.pricediscount)
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "SelectColorCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! SelectColorCell
                cell.selectionStyle = .none
                cell.configureView(variant: self.variant, kategori: self.category)
            default:
                break
            }
        case 2:
            switch indexPath.row {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! DescriptionCell
                cell.selectionStyle = .none
                cell.configureView(desc: self.descripstions)
            default:
                break
            }
        case 3:
            switch indexPath.row {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! EmptyCell
                cell.selectionStyle = .none
            default:
                break
            }
        default:
            break
        }
        return cell!
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                height = 387
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                height = 136
            default:
                break
            }
        case 2:
            switch indexPath.row {
            case 0:
                height = UITableView.automaticDimension
            default:
                break
            }
        case 3:
            switch indexPath.row {
            case 0:
                height = 150
            default:
                break
            }
        default:
            break
        }
        return height
    }
    
    @objc func openListBasket(sender: UIButton) {
        self.navigationController?.pushViewController(ListBasketViewController(), animated: true)
    }
    
    @objc func backView(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    convenience init(id: String) {
        self.init()
        self.id = id
        let mData = SchemeProducts()
        mData.id = self.id
               
        self.arrayProducts = NSMutableArray()
        self.arrayProducts = SQLLiteController.getInstance().getAllItemByID(modelInItem: mData)
        let allData:SchemeProducts = arrayProducts.object(at: 0) as! SchemeProducts
        self.name = allData.name
        self.category = allData.category
        self.variant = allData.variant
        self.price = allData.price
        self.images = allData.images
        self.discount = allData.discount
        self.pricediscount = allData.pricediscount
        self.descripstions = allData.descripstions
        
              
        
    }
    
}
