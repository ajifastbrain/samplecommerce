//
//  HomeViewController.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var textSearch: UITextField!
    
    @IBOutlet weak var btnClearText: UIButton!
    
    @IBOutlet weak var btnLihatSemua: UIButton!
    
    private var arrayItems = NSMutableArray()
    
    private var arrayCountItems = NSMutableArray()
    
    
    @IBOutlet weak var tableItems: UICollectionView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "58b2e1")
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layoutIfNeeded()
        
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Aplikasi Belanja"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: label)
        
        self.arrayCountItems = NSMutableArray()
        self.arrayCountItems = SQLLiteController.getInstance().getCounItems()
        let allData:SchemeTotalItems = arrayCountItems.object(at: 0) as! SchemeTotalItems
       
            
        let numBadge = String(allData.totItems)
          
        if numBadge == "0" {
            let chartButton = SSBadgeButton()
            chartButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
            chartButton.setImage(UIImage(named: "whiteChart")?.withRenderingMode(.alwaysTemplate), for: .normal)
            chartButton.tintColor = .white
            chartButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
            
            let bellButton = SSBadgeButton()
            bellButton.frame = CGRect(x: 30, y: 0, width: 32, height: 32)
            bellButton.setImage(UIImage(named: "bellWhite")?.withRenderingMode(.alwaysTemplate), for: .normal)
            bellButton.tintColor = .white
            bellButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
                
            self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: chartButton), UIBarButtonItem(customView: bellButton)]
        } else {
            let chartButton = SSBadgeButton()
                chartButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                chartButton.setImage(UIImage(named: "whiteChart")?.withRenderingMode(.alwaysTemplate), for: .normal)
                chartButton.tintColor = .white
                chartButton.badge = numBadge
                chartButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
               
                let bellButton = SSBadgeButton()
                bellButton.frame = CGRect(x: 30, y: 0, width: 32, height: 32)
                bellButton.setImage(UIImage(named: "bellWhite")?.withRenderingMode(.alwaysTemplate), for: .normal)
                bellButton.tintColor = .white
                bellButton.addTarget(self, action: #selector(openListBasket(sender:)), for: .touchUpInside)
                   
                self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: chartButton), UIBarButtonItem(customView: bellButton)]

           }
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viewSearch.frame
        rectShape.position = self.viewSearch.center
        rectShape.path = UIBezierPath(roundedRect: self.viewSearch.bounds, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 35, height: 35)).cgPath

        self.viewSearch.layer.backgroundColor = UIColor.init(hexString: "73bee3").cgColor
        self.viewSearch.layer.mask = rectShape
        
        self.textSearch.roundTextField()
        self.textSearch.setLeftPaddingPoints(15)
        self.textSearch.setRightPaddingPoints(15)
        self.textSearch.set(bgColor: UIColor.init(hexString: "abd9f0"), placeholderTxt: "Cari Barang", placeholderColor: .white, txtColor: .white)
        self.textSearch.addTarget(self, action: #selector(onChangeSearch(sender:)), for: .editingChanged)
        self.btnClearText.isHidden = true
        
        self.btnLihatSemua.layer.borderWidth = 1
        self.btnLihatSemua.layer.borderColor = UIColor.white.cgColor
        self.btnLihatSemua.layer.cornerRadius = 15
        
        self.tableItems.register(UINib(nibName: "ItemsCell", bundle: nil), forCellWithReuseIdentifier: "ItemsCell")
               
        self.tableItems.delegate = self
        self.tableItems.dataSource = self
        self.getData()
        self.tableItems.reloadData()
        
    }


    @objc func onChangeSearch(sender: UITextField) {
        if sender.text == "" {
            self.btnClearText.isHidden = true
        } else {
            self.btnClearText.isHidden = false
        }
    }
    
    @objc func openListBasket(sender: UIButton) {
        self.navigationController?.pushViewController(ListBasketViewController(), animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    }
     
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: 189, height: 251)
    }
    
    private func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt indexPath: IndexPath) -> CGFloat {
     return 0.0
    }
    
    private func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt indexPath: IndexPath) -> CGFloat {
     return 0.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemsCell", for: indexPath as IndexPath) as! ItemsCell
        
        let allData:SchemeProducts = arrayItems.object(at: indexPath.row) as! SchemeProducts
        
        cell.configureView(imageName: allData.images, discount: allData.discount, nameItem: allData.name, price: allData.price)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let allData:SchemeProducts = arrayItems.object(at: indexPath.row) as! SchemeProducts
        self.navigationController?.pushViewController(DetailViewController(id: allData.id), animated: true)
    }
    
    func getData() {
        self.arrayItems = NSMutableArray()
        self.arrayItems = SQLLiteController.getInstance().getAllItems()
    }

}
