//
//  ListBasketViewController.swift
//  SampleCommerce
//
//  Created by Macintosh on 18/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ListBasketViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewNav: UIView!
    
    @IBOutlet weak var mainTable: UITableView!
    
    private var arrayItems = NSMutableArray()
    
    @IBOutlet weak var btnPaid: UIButton!
    
    private var arrayCountItems = NSMutableArray()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "58b2e1")
         navigationController?.navigationBar.shadowImage = UIImage()
         navigationController?.navigationBar.layoutIfNeeded()
         self.navigationItem.setHidesBackButton(true, animated: true)
        
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "List Basket"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        self.navigationItem.titleView = label
        
        self.btnPaid.layer.cornerRadius = 8
        self.btnPaid.layer.backgroundColor = UIColor.init(hexString: "58b2e1").cgColor
        self.btnPaid.layer.borderWidth = 1
        self.btnPaid.layer.borderColor = UIColor.white.cgColor
        self.btnPaid.setTitleColor(UIColor.white, for: .normal)
        self.btnPaid.setTitle("Bayar", for: .normal)
        
        let backButton = UIButton()
        backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        backButton.setImage(UIImage(named: "arrowBack")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = .white
        backButton.addTarget(self, action: #selector(backView(sender:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backButton)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viewNav.frame
        rectShape.position = self.viewNav.center
        rectShape.path = UIBezierPath(roundedRect: self.viewNav.bounds, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 35, height: 35)).cgPath

        self.viewNav.layer.backgroundColor = UIColor.init(hexString: "73bee3").cgColor
        self.viewNav.layer.mask = rectShape
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YYYY"
        self.getData(dateTrans: dateFormatter.string(from: date))
        
        self.mainTable.register(UINib(nibName: "ListBasketCell", bundle: nil), forCellReuseIdentifier: "ListBasketCell")
               
        self.mainTable.delegate = self
        self.mainTable.dataSource = self
        self.mainTable.reloadData()
    }


    func getData(dateTrans: String) {
        let mData = SchemeBasket()
        mData.dateTrans = dateTrans
        
        self.arrayItems = NSMutableArray()
        self.arrayItems = SQLLiteController.getInstance().getAllBasket(modelInItem: mData)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListBasketCell", for: indexPath as IndexPath) as UITableViewCell
        let cells = cell as! ListBasketCell
        let allData:SchemeBasket = arrayItems.object(at: indexPath.row) as! SchemeBasket
        cells.selectionStyle = .none
        cells.configureView(imgProd: allData.images, name: allData.name, price: allData.price, diskon: allData.discount, qty: allData.qty)
        return cells
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 143
    }
    
    @objc func backView(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnBayarTapped(_ sender: UIButton) {
        let deleteItems = SQLLiteController.getInstance().deleteItems()
        if deleteItems {
            self.navigationController?.popViewController(animated: true)
        } else {
            print("Items Order Gagal Hapus")
        }
    }
    

}
