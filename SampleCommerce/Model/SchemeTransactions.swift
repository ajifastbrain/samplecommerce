//
//  SchemeTransactions.swift
//  SampleCommerce
//
//  Created by Macintosh on 15/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

class SchemeTransactions: NSObject {
    var id: String = String()
    var qty: Int = Int()
    var dateTrans: String = String()
}
