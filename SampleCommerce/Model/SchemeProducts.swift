//
//  SchemeProducts.swift
//  SampleCommerce
//
//  Created by Macintosh on 07/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

class SchemeProducts: NSObject {
    var id: String = String()
    var name: String = String()
    var category: String = String()
    var variant: String = String()
    var price: Int = Int()
    var images: String = String()
    var discount: Int = Int()
    var pricediscount: Int = Int()
    var descripstions: String = String()
}
