//
//  SchemeBasket.swift
//  SampleCommerce
//
//  Created by Macintosh on 18/12/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

class SchemeBasket: NSObject {
    var images: String = String()
    var name: String = String()
    var price: Int = Int()
    var discount: Int = Int()
    var qty: Int = Int()
    var dateTrans: String = String()
}
